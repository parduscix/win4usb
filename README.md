Kullanımı:

1- Bu projeyi klonlayın

2- İçine windows isoundaki dosyaları binary dizinine kopyalayın. (isoyu açıp kopyala yapıştır yolu ile)

3- binary.sh betiğini çalıştırın.

4- Oluşan Win4usb.iso dosyasını dd veya herhangi bir iso yazan uygulama ile linux isosu yazar gibi yazın.

5- Kullanıma hazır.

Not: UEFI kurulum yapacaksanız bu isoyu unetbootin ile yazdırın.
